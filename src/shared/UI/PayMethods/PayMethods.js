import React from "react";
import { Link } from "react-router-dom";

import styles from "./PayMethods.module.scss";

const PayMethods = props => {
  return props.methods.map(method => (
    <Link key={method.title} to={method.link}>
      <img
        className={styles.payMethod}
        src={require("./../../../assets/images/" + method.url)}
        alt={method.title}
      />
    </Link>
  ));
};

export default PayMethods;
