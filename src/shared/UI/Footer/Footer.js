import React from "react";
import { TiSocialFacebook, TiSocialPinterest } from "react-icons/ti";
import { AiOutlineInstagram } from "react-icons/ai";
import { IoMdHeartEmpty } from "react-icons/io";

import styles from "./Footer.module.scss";
import FooterCol from "./FooterCol/FooterCol";
import PayMethods from "./../PayMethods/PayMethods";
import Copyright from "./Copyright/Copyright";

const Footer = () => {
  return (
    <div className={styles.wrapper}>
      <div className="container">
        <div className="row">
          <FooterCol
            title="Categories"
            links={[
              { title: "Women", link: "women" },
              { title: "Men", link: "men" },
              { title: "Shoes", link: "shoes" },
              { title: "Watches", link: "watches" }
            ]}
          />
          <FooterCol
            title="Help"
            links={[
              { title: "Track Order", link: "trak-order" },
              { title: "Returns", link: "returns" },
              { title: "Shipping", link: "shipping" },
              { title: "FAQs", link: "faqs" }
            ]}
          />
          <FooterCol
            title="Get In Touch"
            socials={[
              {
                title: "facebook",
                link: "facebook",
                icon: <TiSocialFacebook />
              },
              {
                title: "instagram",
                link: "instagram",
                icon: <AiOutlineInstagram />
              },
              {
                title: "pinterest",
                link: "pinterest",
                icon: <TiSocialPinterest />
              }
            ]}
          >
            Any questions? Let us know in store at 8th floor, 379 Hudson St, New
            York, NY 10018 or call us on (+1) 96 716 6879{" "}
          </FooterCol>
          <FooterCol title="Newsletter" type="subscribe"></FooterCol>
        </div>
        <div
          className={`row justify-content-center align-items-center ${styles.payMethods}`}
        >
          <PayMethods
            methods={[
              { title: "PayPal", link: "pay-pal", url: "icon-pay-01.png" },
              { title: "Visa", link: "pay-pal", url: "icon-pay-02.png" },
              {
                title: "Mastercard",
                link: "mastercard",
                url: "icon-pay-03.png"
              },
              {
                title: "Brandovici",
                link: "brandovici",
                url: "icon-pay-04.png"
              },
              { title: "Decover", link: "decover", url: "icon-pay-05.png" }
            ]}
          ></PayMethods>
        </div>
        <div className="text-center">
          <Copyright
            text="Copyright ©2020 All rights reserved | This template is made with by"
            icon={<IoMdHeartEmpty />}
            link={{ title: "Colorlib", url: "colorlib" }}
          />
        </div>
      </div>
    </div>
  );
};
export default Footer;
