import React from "react";
import { Link } from "react-router-dom";

import styles from "./Footer.module.scss";
import Button from "./../../../components/FormElements/Button/Button";

const FooterCol = props => {
  if (props.links !== undefined) {
    return (
      <div className={`${styles.colBox} col-sm-6 col-md-3`}>
        <h4>{props.title}</h4>
        <ul>
          {props.links.map(link => {
            return (
              <li key={link.title}>
                <Link className="transition-all-04" to={`/${link.link}`}>
                  {link.title}
                </Link>
              </li>
            );
          })}
        </ul>
      </div>
    );
  }

  if (props.socials !== undefined) {
    return (
      <div className={`${styles.colBox} col-sm-6 col-md-3`}>
        <h4>{props.title}</h4>
        <p>{props.children}</p>
        <div className={styles.social}>
          {props.socials.map(social => (
            <Link key={social.link} to={`/${social.link}`}>
              {social.icon}
            </Link>
          ))}
        </div>
      </div>
    );
  }
  if (props.subscribe !== "") {
    return (
      <div className={`${styles.colBox} col-sm-6 col-md-3`}>
        <h4>{props.title}</h4>
        <form>
          <div className={styles.inputBox}>
            <input type="email" placeholder="email@example.com"></input>
            <span></span>
          </div>
          <div className={styles.buttonBox}>
            <Button blueButton>Subscribe</Button>
          </div>
        </form>
      </div>
    );
  }
  return null;
};

export default FooterCol;
