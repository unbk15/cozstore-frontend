import React from "react";
import { Link } from "react-router-dom";

import styles from "./Copyright.module.scss";

const Copyright = props => {
  const text = props.text.split(" ");
  const lastWord = text.pop();

  return (
    <p className={styles.copyright}>
      {text}
      <span> {props.icon} </span>
      {lastWord}&nbsp;
      <Link to={props.link.url}>{props.link.title}</Link>
    </p>
  );
};

export default Copyright;
