import React, { Component } from "react";

import Topbar from "./Topbar/Topbar";
import Logo from "./Logo/Logo";
import Navigation from "./Navigation/Navigation";
import Search from "./Search/Search";
import Shoppingcart from "./Shoppingcart/Shoppingcart";
import Favorite from "./Favorite/Favorite";
import Modalsearch from "./Modalsearch/Modalsearch";
import Mobileheader from "./Mobileheader/Mobileheader";
import Menumobile from "./Menumobile/Menumobile";
import Modalcart from "./Modalcart/Modalcart";
import styles from "./Header.module.scss";

class Header extends Component {
  constructor() {
    super();
    this.state = {
      showModalSearch: false,
      showModalCart: false,
      showMenuMobile: false,
      backgroundColor: "transparent",
      displayTopBar: "block",
      boxShadow: "none"
    };
  }
  openModalSearchHandler = () => {
    this.setState({ showModalSearch: true });
  };
  closeModalSearchHandler = () => {
    this.setState({ showModalSearch: false });
  };
  openModalCartHandler = () => {
    this.setState({ showModalCart: true });
  };
  closeModalCartHandler = () => {
    this.setState({ showModalCart: false });
  };

  toggleMenuMobileHandler = () => {
    this.setState({ showMenuMobile: !this.state.showMenuMobile });
  };

  listenScrollEvent = e => {
    if (window.scrollY > 50) {
      this.setState({ backgroundColor: "white" });
      this.setState({ displayTopBar: "none" });
      this.setState({ boxShadow: "0 0 3px 0 rgba(0, 0, 0, 0.2)" });
    } else {
      this.setState({ backgroundColor: "transparent" });
      this.setState({ displayTopBar: "block" });
      this.setState({ boxShadow: "none" });
      
    }
  };

  componentDidMount() {
    window.addEventListener("scroll", this.listenScrollEvent);
  }
  render() {
    return (
      <>
        <header>
          <div className={styles.containerMenuDesktop}>
            <Topbar displayTopBar={this.state.displayTopBar} />
            <div
              className={styles.wrapMenuDesktop}
              style={{
                backgroundColor: this.state.backgroundColor,
                boxShadow: this.state.boxShadow
              }}
            >
              <nav className={`${styles.limiterMenuDesktop} container`}>
                <Logo />
                <Navigation />
                <div
                  className={`${styles.wrapIconHeader} ${styles.flex} ${styles.flexW} ${styles.flexRM}`}
                >
                  <Search openModalSearch={this.openModalSearchHandler} />
                  <Shoppingcart openModalCart={this.openModalCartHandler} />
                  <Favorite />
                </div>
              </nav>
            </div>
          </div>
          <Mobileheader
            toggleMenuMobile={this.toggleMenuMobileHandler}
            openModalSearch={this.openModalSearchHandler}
            openModalCart={this.openModalCartHandler}
          />
          <Menumobile
            showMenuMobile={this.state.showMenuMobile}
            toggleMenuMobile={this.toggleMenuMobileHandler}
          />
          <Modalsearch
            showModalSearch={this.state.showModalSearch}
            closeModalSearch={this.closeModalSearchHandler}
          />
          <Modalcart
            showModalCart={this.state.showModalCart}
            closeModalCart={this.closeModalCartHandler}
          />
        </header>
      </>
    );
  }
}

export default Header;
