import React from "react";
import { MdSearch } from "react-icons/md";

import styles from "./Search.module.css";

const Search = props => {
  return (
    <div
      onClick={props.openModalSearch}
      className={`${styles.iconHeaderItem} ${styles.cl2}  ${styles.trans04} ${styles.pR11} ${styles.pL22} js-show-modal-search`}
    >
      <i className={`${styles.zmdi} ${styles.zmdiSearch}`}>
        <MdSearch />
      </i>
    </div>
  );
};

export default Search;
