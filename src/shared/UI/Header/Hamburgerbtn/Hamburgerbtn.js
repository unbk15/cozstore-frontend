import React from "react";

import styles from "./Hamburgerbtn.module.css";

const Hamburgerbtn = props => {
  return (
    <div
      onClick={props.toggleMenuMobile}
      className={`${styles.hamburger} ${styles.hamburgerSqueeze}`}
    >
      <span className={styles.hamburgerBox}>
        <span className={styles.hamburgerInner}></span>
      </span>
    </div>
  );
};

export default Hamburgerbtn;
