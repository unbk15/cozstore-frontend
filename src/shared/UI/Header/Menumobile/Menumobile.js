import React from "react";

import { NavLink } from "react-router-dom";
import styles from "./Menumobile.module.css";

const Menumobile = props => {
  if (props.showMenuMobile) {
    return (
      <div className={styles.menuMobile}>
        <ul className={styles.topbarMobile}>
          <li>
            <div className={styles.leftTopBar}>
              Free shipping for standard order over $100
            </div>
          </li>
          <li>
            <div
              className={`${styles.flex} ${styles.rightTopBar} ${styles.flexW} ${styles.hFull}`}
            >
              <NavLink
                to="/"
                className={`${styles.flex} ${styles.flexCM} ${styles.pLR10} ${styles.trans04}`}
              >
                Help & FAQs
              </NavLink>

              <NavLink
                to="/"
                className={`${styles.flex} ${styles.flexCM} ${styles.pLR10} ${styles.trans04}`}
              >
                My Account
              </NavLink>

              <NavLink
                to="/"
                className={`${styles.flex} ${styles.flexCM} ${styles.pLR10} ${styles.trans04}`}
              >
                EN
              </NavLink>

              <NavLink
                to="/"
                className={`${styles.flex} ${styles.flexCM} ${styles.pLR10} ${styles.trans04}`}
              >
                USD
              </NavLink>
            </div>
          </li>
        </ul>
        <ul className={styles.mainMenuM}>
          <li>
            <NavLink to="/" onClick={props.toggleMenuMobile}>
              Home
            </NavLink>
          </li>

          <li>
            <NavLink to="/product" onClick={props.toggleMenuMobile}>
              Shop
            </NavLink>
          </li>
          <li>
            <NavLink
              to="/shoping-cart"
              onClick={props.toggleMenuMobile}
              className={`${styles.label1} ${styles.rs1}`}
              data-label1="hot"
            >
              Features
            </NavLink>
          </li>
          <li>
            <NavLink to="/blog" onClick={props.toggleMenuMobile}>
              Blog
            </NavLink>
          </li>
          <li>
            <NavLink to="/about" onClick={props.toggleMenuMobile}>
              About
            </NavLink>
          </li>
          <li>
            <NavLink to="/contact" onClick={props.toggleMenuMobile}>
              Contact
            </NavLink>
          </li>
        </ul>
      </div>
    );
  } else {
    return null;
  }
};

export default Menumobile;
