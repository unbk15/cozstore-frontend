import React from "react";
import { MdSearch } from "react-icons/md";

import closeImage from "./../../../../assets/images/icon-close.png";
import styles from "./Modalsearch.module.css";

const Modalsearch = props => {
  if (props.showModalSearch) {
    return (
      <div
        className={`${styles.modalSearchHeader} ${styles.flex} ${styles.flexCM} ${styles.trans04} js-hide-modal-search`}
      >
        <div className={styles.containerSearchHeader}>
          <button
            onClick={props.closeModalSearch}
            className={`${styles.button} ${styles.flex} ${styles.flexCM} ${styles.btnHideModalSearch} ${styles.trans04} js-hide-modal-search`}
          >
            <img src={closeImage} alt="CLOSE" />
          </button>

          <form
            className={`${styles.wrapSearchHeader} ${styles.flex} ${styles.flexW} ${styles.pL15}`}
          >
            <button
              className={`${styles.flex} ${styles.flexCM} ${styles.trans04}`}
            >
              <MdSearch className={styles.zmdi} />
            </button>
            <input
              className="plh3"
              type="text"
              name="search"
              placeholder="Search..."
            ></input>
          </form>
        </div>
      </div>
    );
  } else {
    return null;
  }
};
export default Modalsearch;
