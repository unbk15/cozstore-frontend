import React from "react";
import { MdClose } from "react-icons/md";
import ItemCart01 from "./../../../../assets/images/item-cart-01.jpg";
import ItemCart02 from "./../../../../assets/images/item-cart-02.jpg";
import ItemCart03 from "./../../../../assets/images/item-cart-03.jpg";
import { Link } from "react-router-dom";

import styles from "./Modalcart.module.css";

const Cart = props => {
  if (props.showModalCart) {
    return (
      <div className={`${styles.wrapHeaderCart}`}>
        <div className={`${styles.sFull}`} onClick={props.closeModalCart}></div>
        <div
          className={`${styles.headerCart} ${styles.flex} ${styles.flexColL} ${styles.pL65} ${styles.pR25}`}
        >
          <div
            className={`${styles.headerCartTitle} ${styles.flex} ${styles.flexW} ${styles.flexSbM} ${styles.pB8}`}
          >
            <span className={`${styles.mtext103} ${styles.cl2}`}>
              Your Cart
            </span>
            <div
              onClick={props.closeModalCart}
              className={`${styles.fS35} ${styles.lH10} ${styles.cl2} ${styles.pLR5} pointer ${styles.hovCl1} ${styles.trans04}`}
            >
              <MdClose />
            </div>
          </div>

          <div
            className={`${styles.headerCartContent} ${styles.flex} ${styles.flexW} ${styles.pS}`}
          >
            <ul className={`${styles.headerCartWrapItem} ${styles.wFull}`}>
              <li
                className={`${styles.headerCartItem} ${styles.flex} ${styles.flexW} ${styles.flexT} ${styles.mB12}`}
              >
                <div className={`${styles.headerCartItemImg}`}>
                  <img src={ItemCart01} alt="IMG" />
                </div>

                <div className={`${styles.headerCartItemTxt} ${styles.pT8}`}>
                  <Link
                    to="/"
                    className={`${styles.headerCartItemName} ${styles.mB18} ${styles.hovCl1} ${styles.trans04}`}
                  >
                    White Shirt Pleat
                  </Link>

                  <span className={styles.headerCartItemInfo}>1 x $19.00</span>
                </div>
              </li>
              <li
                className={`${styles.headerCartItem} ${styles.flex} ${styles.flexW} ${styles.flexT} ${styles.mB12}`}
              >
                <div className={`${styles.headerCartItemImg}`}>
                  <img src={ItemCart02} alt="IMG" />
                </div>

                <div className={`${styles.headerCartItemTxt} ${styles.pT8}`}>
                  <Link
                    to="/"
                    className={`${styles.headerCartItemName} ${styles.mB18} ${styles.hovCl1} ${styles.trans04}`}
                  >
                    Convers All Star
                  </Link>

                  <span className={styles.headerCartItemInfo}>1 x $39.00</span>
                </div>
              </li>
              <li
                className={`${styles.headerCartItem} ${styles.flex} ${styles.flexW} ${styles.flexT} ${styles.mB12}`}
              >
                <div className={`${styles.headerCartItemImg}`}>
                  <img src={ItemCart03} alt="IMG" />
                </div>

                <div className={`${styles.headerCartItemTxt} ${styles.pT8}`}>
                  <Link
                    to="/"
                    className={`${styles.headerCartItemName} ${styles.mB18} ${styles.hovCl1} ${styles.trans04}`}
                  >
                    Nixon Porter Leather
                  </Link>

                  <span className={styles.headerCartItemInfo}>1 x $17.00</span>
                </div>
              </li>
            </ul>

            <div className={`${styles.wFull}`}>
              <div
                className={`${styles.headerCartTotal} ${styles.wFull} ${styles.pTB40}`}
              >
                Total: $75.00
              </div>

              <div
                className={`${styles.flex} ${styles.headerCartButtons} ${styles.flexW} ${styles.wFull}`}
              >
                <Link
                  to="shoping-cart.html"
                  className={`${styles.flex} ${styles.CM} ${styles.flexCM} ${styles.sText101} ${styles.cl0} ${styles.size107} ${styles.bg3} ${styles.bor2} ${styles.hovBtn3} ${styles.pLR15} ${styles.trans04} ${styles.mR8} ${styles.mB10}`}
                >
                  View Cart
                </Link>
                <Link
                  to="shoping-cart.html"
                  className={`${styles.flex} ${styles.CM} ${styles.flexCM} ${styles.sText101} ${styles.cl0} ${styles.size107} ${styles.bg3} ${styles.bor2} ${styles.hovBtn3} ${styles.pLR15} ${styles.trans04} ${styles.mR8} ${styles.mB10}`}
                >
                  Check Out
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    return null;
  }
};

export default Cart;
