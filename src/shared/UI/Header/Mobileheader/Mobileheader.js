import React from "react";

import Logo from "./../Logo/Logo";
import Search from "./../Search/Search";
import Shoppingcart from "./../Shoppingcart/Shoppingcart";
import Favorite from "./../Favorite/Favorite";
import Hamburgerbtn from "./../Hamburgerbtn/Hamburgerbtn";
// import Menumobile from "./../Menumobile/Menumobile";
import styles from "./Mobileheader.module.css";

const Mobileheader = props => {
  return (
    <div className={styles.wrapHeaderMobile}>
      <div className={styles.logoMobile}>
        <Logo />
      </div>
      <div
        className={`${styles.wrapIconHeader} ${styles.flex} ${styles.flexW} ${styles.mR15}`}
      >
        <Search openModalSearch={props.openModalSearch} />
        <Shoppingcart openModalCart={props.openModalCart} />
        <Favorite />
      </div>
      <Hamburgerbtn toggleMenuMobile={props.toggleMenuMobile} />
    </div>
  );
};

export default Mobileheader;
