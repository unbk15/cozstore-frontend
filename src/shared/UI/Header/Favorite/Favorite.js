import React from "react";
import { Link } from "react-router-dom";
import { MdFavoriteBorder } from "react-icons/md";

import styles from "./Favorite.module.css";

const Favorite = () => {
  return (
    <Link
      to="/"
      className={`${styles.disBlock} ${styles.iconHeaderItem} ${styles.cl2} ${styles.pR11} ${styles.pL22} ${styles.trans04} ${styles.iconHeaderNoti}`}
      data-notify="5"
    >
      <i className={`${styles.zmdi} ${styles.zmdiFavoriteOutline}`}>
        <MdFavoriteBorder />
      </i>
    </Link>
  );
};

export default Favorite;
