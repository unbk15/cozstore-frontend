import React from "react";
import { Link } from "react-router-dom";

import logoImage from "./../../../../assets/images/logo-desktop.png";
import styles from "./Logo.module.css";

const Logo = () => {
  return (
    <Link to="/" className={`${styles.logo}`}>
      <img src={logoImage} alt="Logo" />
    </Link>
  );
};
export default Logo;
