import React from "react";
import { NavLink } from "react-router-dom";
import uuid from "uuid/v4";
import styles from "./Navigation.module.scss";

const navItems = [{title : "Home", to: "/"}, 
                  {title : "Shop", to: "/shop"}, 
                  {title : "Features", to: "/features", label: "hot"},
                  {title : "Blog", to: "/blog"},
                  {title : "About", to: "/about"},
                  {title : "Contact", to: "/contact"}
                ]

const Navigation = () => {
  return (
    <div className={styles.menuDesktop}>
      <ul className={styles.mainMenu}>
        {navItems.map(navItem => {
          return (<li key={uuid()} className={navItem.label && styles.label1} data-label1={navItem.label && navItem.label}>
          <NavLink to={navItem.to} activeClassName="active-menu">
            {navItem.title}
          </NavLink>
        </li>)
        })}
      </ul>
    </div>
  );
};

export default Navigation;
