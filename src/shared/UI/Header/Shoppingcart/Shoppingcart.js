import React from "react";
import { MdShoppingCart } from "react-icons/md";

import styles from "./Shoppingcart.module.css";

const Search = props => {
  return (
    <div
      onClick={props.openModalCart}
      className={`${styles.iconHeaderItem} ${styles.cl2}  ${styles.trans04} ${styles.pR11} ${styles.pL22} ${styles.iconHeaderNoti} js-show-cart`}
      data-notify="2"
    >
      <i className={`${styles.zmdi} ${styles.zmdiShoppingCart}`}>
        <MdShoppingCart />
      </i>
    </div>
  );
};

export default Search;
