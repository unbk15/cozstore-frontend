import React from "react";
import { NavLink } from "react-router-dom";
import styles from "./Topbar.module.css";

const Topbar = props => {
  return (
    <div className={styles.topBar} style={{ display: props.displayTopBar }}>
      <div
        className={`content-topbar container ${styles.flexSbM} ${styles.hFull} ${styles.flex}`}
      >
        <div className={styles.leftTopBar}>
          Free shipping for standard order over $100
        </div>

        <div
          className={`${styles.rightTopBar} ${styles.flex} ${styles.flexW} ${styles.hFull}`}
        >
          <NavLink
            to="/"
            className={`${styles.flex} ${styles.flexCM} ${styles.trans04} ${styles.pLR25}`}
          >
            Help & FAQs
          </NavLink>

          <NavLink
            to="/"
            className={`${styles.flex} ${styles.flexCM} ${styles.trans04} ${styles.pLR25}`}
          >
            My Account
          </NavLink>

          <NavLink
            to="/"
            className={`${styles.flex} ${styles.flexCM} ${styles.trans04} ${styles.pLR25}`}
          >
            EN
          </NavLink>

          <NavLink
            to="/"
            className={`${styles.flex} ${styles.flexCM} ${styles.trans04} ${styles.pLR25}`}
          >
            USD
          </NavLink>
        </div>
      </div>
    </div>
  );
};

export default Topbar;
