import React from "react";
import { Link } from "react-router-dom";

import { IoMdHeartEmpty } from "react-icons/io";
import styles from "./Product.module.scss";

const Product = props => {
  return (
    <>
      <div className="col-sm-6 col-md-4 col-lg-3 pb-4">
        <div>
          <div className={`${styles.imageBox} position-relative`}>
            <img
              className="w-100"
              src={`http://localhost:8000/${props.product.cover}`}
              alt={props.product.title}
            />
            <Link
              to="/"
              className={`${styles.button}`}
              onClick={() => props.toggleModal(props.product)}
            >
              Quick View
            </Link>
          </div>
          <div className="pt-2 d-flex justify-content-between">
            <div>
              <Link className={`${styles.link} mb-2`} to="/">
                {props.product.title}
              </Link>
              <p className={`${styles.price} m-0`}>${props.product.price}</p>
            </div>
            <div>
              <IoMdHeartEmpty className={styles.icon} />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Product;
