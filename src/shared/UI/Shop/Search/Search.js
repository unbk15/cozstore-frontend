import React from "react";

import { MdSearch } from "react-icons/md";
import styles from "./Search.module.scss";

const Search = props => {
  const duration = 300;

  const defaultStyle = {
    transtion: `opacity ${duration}ms ease-in-out`
  };

  const transitionStyle = {
    entering: { opacity: 0 },
    entered: { opacity: 1 },
    exiting: { opacity: 1 },
    exited: { opacity: 0 }
  };

  if (props.showSearch) {
    return (
      <form
        className="position-relative"
        style={{ ...defaultStyle, ...transitionStyle[props.state] }}
        onSubmit={event => event.preventDefault()}
      >
        <input
          className={`${styles.search}`}
          type="text"
          placeholder="Search"
          value={props.query}
          onChange={event => props.onChange(event)}
        ></input>
        <button className={styles.searchSubmit} type="submit">
          <MdSearch />
        </button>
      </form>
    );
  } else {
    return null;
  }
};

export default Search;
