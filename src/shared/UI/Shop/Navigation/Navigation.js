import React from "react";
import { Link } from "react-router-dom";
import _lodash from "lodash";

import styles from "./Navigation.module.scss";

const Navigation = props => {
  const categories = props.categories.map(category => category.category);
  return (
    <ul className={styles.navigation}>
       <li>
        <Link
          to="#"
          className={styles.link}
          onClick={() => props.filterProducts()}
        >
          All Products
        </Link>
      </li>
      {categories.map(category => {
          return (
            <li key={category._id}>
              <Link
                to="#"
                className={styles.link}
                onClick={() => props.filterProducts(category._id)}
              >
                {_lodash.upperFirst(category.category)}
              </Link>
            </li>
          );
        })}
    </ul>
  );
};

export default Navigation;
