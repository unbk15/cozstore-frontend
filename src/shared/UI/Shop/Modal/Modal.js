import React from "react";
import Slider from "react-slick";
import { MdClose } from "react-icons/md";
import styles from "./Modal.module.scss";

const Modal = props => {
  if (props.showModal) {
    const settings = {
      // customPaging: function(i) {
      //   return (
      //     <a>
      //       <img src={`${baseUrl}/abstract0${i + 1}.jpg`} />
      //     </a>
      //   );
      // },
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    return (
      <div className={`${styles.modal}`}>
        <div className={`${styles.modalInside} container pt-5 pb-5`}>
          <MdClose
            className={`${styles.closeIcon}`}
            onClick={props.toggleModal}
          />
          <div className={`${styles.modalProduct}`}>
            <div className="row">
              <div className="col-md-6">
                <Slider {...settings}>
                  {props.product.images.map(image => {
                    return (
                      <div key={image.id}>
                        <img
                          src={require("./../../../../assets/images/" +
                            image.src)}
                          alt={props.product.title}
                        />
                      </div>
                    );
                  })}
                </Slider>
              </div>
              <div className="col-md-6">
                <h3>{props.product.title}</h3>
                <p>{props.product.description}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    return null;
  }
};

export default Modal;
