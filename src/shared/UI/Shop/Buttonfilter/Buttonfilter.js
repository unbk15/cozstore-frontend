import React from "react";
import { Button } from "reactstrap";
import { MdFilterList, MdSearch } from "react-icons/md";

import styles from "./Buttonfilter.module.scss";

const Buttonfilter = props => {
  return (
    <Button
      className={`${styles.button} ${props.styles}`}
      onClick={props.toggleFilter ? props.toggleFilter : props.toggleSearch}
    >
      {props.icon === "MdFilterList" ? (
        <MdFilterList className={`${styles.buttonIcon} mr-1`} />
      ) : (
        <MdSearch className={`${styles.buttonIcon} mr-1`} />
      )}
      {props.name}
    </Button>
  );
};

export default Buttonfilter;
