import React, { Component } from "react";
import { connect } from "react-redux";
import {
  storeProducts,
  storeFilterProducts, 
  searchProducts,
  advanceSearchFilter
} from "./../../../store/actions/index";

import Spinner from "./../Spinner/Spinner";
import Navigation from "./Navigation/Navigation";
import Buttonfilter from "./Buttonfilter/Buttonfilter";
import Filters from "./Filters/Filters";
import Search from "./Search/Search";
import Product from "./Product/Product";
import Modal from "./Modal/Modal";

import products from "./../../../assets/data/products.json";

import { Transition } from "react-transition-group";
import styles from "./Shop.module.scss";

class Shop extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      showMoreButton:true,
      products,
      categories: [],
      product: {},
      searchWord: "",
      showFilters: false,
      showSearch: false,
      showModal: false,
      displayHeading: this.props.diplayHeading
        ? this.props.displayHeading
        : "block"
    };
    this.timer = null;
  }

  componentDidMount() {
    this.props.OnFetchingProducts();
  }
   
  componentWillReceiveProps(nextProps){

    let productsArray     = [...new Map(this.props.prods.map(item => [item.category.category, item])).values()];
    let nextProductsArray = [...new Map(nextProps.prods.map(item => [item.category.category, item])).values()];
    if(nextProductsArray.length > productsArray.length){
      this.setState( (prevState) => {  
        return {categories : nextProductsArray }});
    }
  }

  onChangeHandler = event => {
    clearTimeout(this.timer);
    const searchWord = event.target.value;
    this.setState((prevState, event) => ({searchWord}));
    this.timer =  setTimeout(()=>{
      this.props.OnFetchingSearchProducts(this.state.searchWord);
    }, 1000);

  };

  filterProductsHandler = filterSelectorId => {
    if (filterSelectorId) {
      this.props.OnFetchingFilterProducts(filterSelectorId);
    } else {
      this.props.OnFetchingProducts();
    }
  };

  toggleFilterHandler = () => {
    if (this.state.showSearch) {
      this.setState({ showSearch: false });
    }
    this.setState({ showFilters: !this.state.showFilters });
  };

  toggleSearchHandler = () => {

    if (this.state.showFilters) {
      this.setState({ showFilters: false });
    }
    this.setState({ showSearch: !this.state.showSearch });
  };

  toggleModalHandler = product => {
    this.setState({ showModal: !this.state.showModal });
    this.setState({ product });
  };

  // Filters Methods 

  filtersValuesHandler = (values) =>{
    console.log(values);
    let minPrice;
    let maxPrice;
    let color = "";
    let tag   = "";

    // 1) Getting the min and max Price
    if(values.price){
      let prices = values.price.split("-").map(price => parseInt(price));
      if(prices.length === 1){
        minPrice = prices[0];
      }else{
        minPrice = prices[0];
        maxPrice = prices[1];
      }
    }
    // 2) Getting colors
      if(values.black) color += values.black + ",";
      if(values.blue)  color += values.blue + ",";
      if(values.grey)  color += values.grey + ",";
      if(values.green) color += values.green + ",";
      if(values.white) color += values.white + ","
    // 3) Getting Tags 
      if(values.fashion) tag += values.fashion + ",";
      if(values.lifeStyle) tag += values.lifeStyle + ",";
      if(values.denim) tag += values.denim + ",";
      if(values.streetStyle) tag += values.streetStyle + ",";
      if(values.crafts) tag += values.crafts + ",";
    // Sending the object filter.
     let filter  = {
        limit: this.state.limit,
        sort: values.sortby,
        minPrice, 
        maxPrice,
        color: color.slice(0, -1),
        tag: tag.slice(0, -1)
     }
    this.props.OnFetchingAdvanceFilter(filter);
  }

  render() {

  // Getting Products

  let searchResults;
  if(this.props.prods.length > 0 ){
      searchResults = this.props.prods.slice(0, this.state.limit).map(product => {
        return ( <Product key={product._id} product={product} toggleModal={this.toggleModalHandler} />
        )
    })
  }

  if(this.props.prods.length === 0 && this.props.emptyArr === false){
    searchResults = (<Spinner/>);
  }
  if(this.props.prods.length === 0 && this.props.emptyArr && !this.props.error){
    searchResults = (<p className="flex-grow-1 text-center">No Results found</p>);
    }
  if(this.props.prods.length === 0 && this.props.emptyArr && this.props.error){
    searchResults = (<p  className="flex-grow-1 text-center">Ups, Something went wrong : (</p>);
  }

    return (
      <section className={styles.shopSection}>
        <div className="container">
          <h3
            style={{ display: this.displayHeading }}
            className={`${styles.headingSection}`}
          >
            Product Overview
          </h3>
          {!this.props.error && this.props.prods.length > 0 &&  (<div className={styles.filterNavigation}>
            <Navigation
              filterProducts={this.filterProductsHandler}
              categories={this.state.categories}
            />
            <div>
              <Buttonfilter
                toggleFilter={this.toggleFilterHandler}
                name="Filter"
                icon="MdFilterList"
                styles="mr-1"
              />
              <Buttonfilter
                toggleSearch={this.toggleSearchHandler}
                name="Search"
                icon="MdSearch"
              />
            </div>
          </div>
          )}
          <Transition in={this.state.showFilters} timeout={300}>
            {state => (
              <Filters state={state} showFilters={this.state.showFilters} filtersValues={this.filtersValuesHandler} />
            )}
          </Transition>
          <Transition in={this.state.showSearch} timeout={300}>
            {state => (
              <Search
                onChange={this.onChangeHandler}
                state={state}
                showSearch={this.state.showSearch}
                query={this.state.query}
              />
            )}
          </Transition>
          <div className="row mt-4">
           {searchResults}
          </div>
          </div>
        <Modal
          showModal={this.state.showModal}
          toggleModal={this.toggleModalHandler}
          product={this.state.product}
        />
      </section>
    );
  }
}

const mapStateToProps = state => {
  console.log(state);
  return {
    prods: state.prods.products,
    emptyArr: state.prods.emptyArray,
    error: state.prods.error
  };
};

const mapDispatchToProps = dispatch => {
  return {
    OnFetchingProducts: () => dispatch(storeProducts()),
    OnFetchingFilterProducts: (filterSelectorId) =>
      dispatch(storeFilterProducts(filterSelectorId)),
    OnFetchingSearchProducts: (searchWord) => dispatch(searchProducts(searchWord)),
    OnFetchingAdvanceFilter: (searchFilters) => dispatch(advanceSearchFilter(searchFilters))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Shop);
