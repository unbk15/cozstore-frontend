import React from "react";

import styles from "./Tag.module.scss";

const Tag = props => {
  return (
    <div className="d-flex position-relative">
      <input
        className={`${styles.tag}`}
        id={props.id}
        type="checkbox"
        name={props.name}
        value={props.value}
      ></input>
      <label htmlFor={props.id} className={`${styles.label} mb-2 mr-2`}>
        {props.label}
      </label>
    </div>
  );
};

export default Tag;
