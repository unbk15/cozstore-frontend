import React from "react";

import styles from "./Radiobutton.module.scss";

const RadioButton = props => {
  const propStyle = props.class ? props.class : "";

  return (
    <div className={`d-flex ${propStyle}`}>
      <input
        className={`${styles.input} mr-2`}
        id={props.id}
        type="radio"
        name={props.name}
        value={props.value}
        checked={props.checked}
      ></input>
      <label htmlFor={props.id} className={`${styles.label} m-0`}>
        {props.label}
      </label>
    </div>
  );
};

export default RadioButton;
