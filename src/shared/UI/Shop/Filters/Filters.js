import React, {useEffect,useState} from "react";
import Radiobutton from "./Radiobutton/Radiobutton";
import Checkbox from "./Checkbox/Checkbox";
import Tag from "./Tag/Tag";

import styles from "./Filters.module.scss";

const duration = 300;
const defaultStyle = {
  transition: `opacity ${duration}ms ease-in-out`
};
const transitionStyles = {
  entering: { opacity: 0 },
  entered: { opacity: 1 },
  exiting: { opacity: 1 },
  exited: { opacity: 0 }
};

const Filters = props => {

  const [sortby, setSortBy]           = useState('');
  const [price, setPrice]             = useState('');
  const [black, setBlack]             = useState('');
  const [blue, setBlue]               = useState('');
  const [grey, setGrey]               = useState('');
  const [green, setGreen]             = useState('');
  const [white, setWhite]             = useState('');
  const [fashion, setFashion]         = useState('');
  const [lifeStyle, setLifeStyle]     = useState('');
  const [denim, setDenim]             = useState('');
  const [streetStyle, setStreetStyle] = useState('');
  const [crafs, setCrafs]             = useState('');

  useEffect(()=>{ 
    if(sortby || price || black || blue || grey || green || white || fashion || lifeStyle || denim || streetStyle || crafs){
      props.filtersValues({sortby, price, black, blue, grey, green, white, fashion, lifeStyle, denim, streetStyle, crafs});
    }
    },[sortby, price, black, blue, grey, green, white, fashion, lifeStyle, denim, streetStyle, crafs]);
  

  const filtersHandler = (event) =>{
    
      let filterName  = event.target.name;
      let filterValue = event.target.value;
      let checkStatus = event.target.checked;

      if(filterName  === 'sortby') setSortBy(filterValue);
      if(filterName  === 'price') setPrice(filterValue);
      if(filterName  === 'black'        && checkStatus === true)  setBlack(filterValue);
      if(filterName  === 'black'        && checkStatus === false) setBlack('');
      if(filterName  === 'blue'         && checkStatus === true)  setBlue(filterValue);
      if(filterName  === 'blue'         && checkStatus === false) setBlue('');
      if(filterName  === 'grey'         && checkStatus === true)  setGrey(filterValue);
      if(filterName  === 'grey'         && checkStatus === false) setGrey('');
      if(filterName  === 'green'        && checkStatus === true)  setGreen(filterValue);
      if(filterName  === 'green'        && checkStatus === false) setGreen('');
      if(filterName  === 'white'        && checkStatus === true)  setWhite(filterValue);
      if(filterName  === 'white'        && checkStatus === false) setWhite('');
      if(filterName  === 'fashion'      && checkStatus === true)  setFashion(filterValue);
      if(filterName  === 'fashion'      && checkStatus === false) setFashion('');
      if(filterName  === 'lifeStyle'    && checkStatus === false) setLifeStyle('');
      if(filterName  === 'lifeStyle'    && checkStatus === true)  setLifeStyle(filterValue);
      if(filterName  === 'denim'        && checkStatus === true)  setDenim(filterValue);
      if(filterName  === 'denim'        && checkStatus === false) setDenim('');
      if(filterName  === 'streetStyle'  && checkStatus === true)  setStreetStyle(filterValue);
      if(filterName  === 'streetStyle'  && checkStatus === false) setStreetStyle('');
      if(filterName  === 'crafts'       && checkStatus === true)  setCrafs(filterValue);
      if(filterName  === 'crafts'       && checkStatus === false) setCrafs('');
  }

  if (props.showFilters) {
    return (
      <div
        className={`${styles.filter}`}
        style={{ ...defaultStyle, ...transitionStyles[props.state] }}
      >
        <div className={`${styles.wraper} row`}>
          <div className="col-md-6 col-lg-3">
            <h6 className={`font-weight-bold pb-3 mb-0 pt-2`}>Sort By</h6>
            <div onChange={filtersHandler}>
              <Radiobutton
                id="popularity"
                value="popularity"
                label="Popularity"
                name="sortby"
                class="pb-2"
              />
              <Radiobutton
                id="newness"
                value="newness"
                label="Newness"
                name="sortby"
                class="pb-2"
              />
              <Radiobutton
                id="low_to_heigh"
                value="price"
                label="Low to Heigh"
                name="sortby"
                class="pb-2"
              />
              <Radiobutton
                id="high_to_low"
                value="-price"
                label="High to Low"
                name="sortby"
              />
            </div>
          </div>
          <div className="col-md-6 col-lg-3">
            <h6 className={`font-weight-bold pb-3 mb-0 pt-2`}>Price</h6>
            <div onChange={filtersHandler}>
              <Radiobutton
                id="05"
                value="0-10"
                label="$0.00-$10.00"
                name="price"
                class="pb-2"
              />
              <Radiobutton
                id="50"
                value="10-20"
                label="$10.00-$20.00"
                name="price"
                class="pb-2"
              />
              <Radiobutton
                id="100"
                value="20-50"
                label="$20.00-$50.00"
                name="price"
                class="pb-2"
              />
              <Radiobutton
                id="150"
                value="50-100"
                label="$50.00-$100.00"
                name="price"
                class="pb-2"
              />
              <Radiobutton id="200" value="100" label="$100.00+" name="price" />
            </div>
          </div>
          <div className="col-md-6 col-lg-3">
            <h6 className={`font-weight-bold pb-3 mb-0 pt-2`}>Color</h6>
            <div onChange={filtersHandler}>
              <Checkbox id="black"  value="black" label="Black" name="black" class="pb-2" />
              <Checkbox id="blue"   value="blue"  label="Blue"  name="blue" class="pb-2" />
              <Checkbox id="grey"   value="grey"  label="Grey"  name="grey" class="pb-2" />
              <Checkbox id="green"  value="green" label="Green" name="green" class="pb-2" />
              <Checkbox id="white"  value="white" label="White" name="white" />
            </div>
          </div>
          <div className="col-md-6 col-lg-3">
            <h6 className={`font-weight-bold pb-3 m-0 pt-2`}>Tags</h6>
            <div className="d-inline-flex flex-wrap" onChange={filtersHandler}>
              <Tag id="fashion"     name="fashion"      label="Fashion"       value="fashion" />
              <Tag id="life_style"  name="lifeStyle"    label="Lifestyle"     value="life-style" />
              <Tag id="denim"       name="denim"        label="Denim"         value="denim" />
              <Tag id="streetstyle" name="streetStyle"  label="Streetstyle"   value="street-style" />
              <Tag id="crafts"      name="crafts"       label="Crafts"        value="crafts" />
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    return null;
  }
};

export default Filters;
