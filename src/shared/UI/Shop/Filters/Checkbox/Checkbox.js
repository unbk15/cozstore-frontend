import React from "react";

import styles from "./Checkbox.module.scss";

const Colorcheckbox = props => {
  const propStyle = props.class ? props.class : "";
  return (
    <div className={`d-flex ${propStyle}`}>
      <input
        className={`${styles.input} mr-2`}
        id={props.id}
        type="checkbox"
        name={props.name}
        value={props.value}
      ></input>
      <label htmlFor={props.id} className={`${styles.label} m-0`}>
        {props.label}
      </label>
    </div>
  );
};

export default Colorcheckbox;
