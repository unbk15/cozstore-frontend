import React from "react";
import Slider from "react-slick";
import { Link } from "react-router-dom";
import { Button } from "reactstrap";
import uuid from "uuid/v4";

import slides from "./../../../../assets/data/slider-slides.json";
import styles from "./Slickslider.module.scss";

const SlickSlider = () => {
  let settings = {
    dots: false,
    infinite: true,
    autoplay: false,
    autoplaySpeed: 2000,
    adaptiveHeight: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  };

  return (
    <Slider {...settings}>
      {
        slides.map(slide => {
        return (<div key={uuid()}  className={styles.slickSlide}>
          <div className={`${styles.slideContainer} ${styles.pT150} container`}>
            <div>
            <div>
              <span className={`${styles.lText101} ${styles.cl2}`}>
                {slide.title}
              </span>
            </div>
            <div className={`${styles.pB43} ${styles.pT19}`}>
                <h2 className={styles.lText201}>{slide.subtitle}</h2>
            </div>
            <div>
              <Link to="/product">
                <Button color="primary" className={styles.button}>
                 {slide.text}
                </Button>
              </Link>
            </div>
          </div>
          </div>
          <img src={require(`./../../../../assets/images/${slide.url}`)} alt="IMG" />
        </div>)})}
    </Slider>
  );
};

export default SlickSlider;
