import React from "react";

import { Link } from "react-router-dom";
import styles from "./Banner.module.scss";

const Banner = props => {
  return (
    <div className="col-md-6 col-xl-4 pb-4">
      <div className={`${styles.border} position-relative`}>
        <img className="w-100" src={props.image} alt="IMG" />
        <Link
          to="/"
          className={`${styles.link} position-absolute p-4 d-flex flex-column align-items-start justify-content-between`}
        >
          <div>
            <h3 className={`${styles.title}`}>
              {props.title}
            </h3>
            <h5 className={`${styles.subtitle}`}>{props.subtitle}</h5>
          </div>
          <div className={styles.shopLinkParent}>
            <div className={`${styles.shopLink} text-uppercase`}>Shop Now</div>
          </div>
        </Link>
      </div>
    </div>
  );
};

export default Banner;
