import React, { Component } from "react";

import banner01 from "./../../assets/images/banner-01.jpg";
import banner02 from "./../../assets/images/banner-02.jpg";
import banner03 from "./../../assets/images/banner-03.jpg";
import Banner from "./components/Banner/Banner";
import Shop from "./../../shared/UI/Shop/Shop";
import Slickslider from "./components/Slickslider/Slickslider.js";
import styles from "./Home.module.css";

class Home extends Component {
  render() {
    return (
      <>
        <Slickslider />
        <section className={styles.bannerSection}>
          <div className="container">
            <div className="row justify-content-center">
              <Banner image={banner01} title="Women" subtitle="Spring 2018" />
              <Banner image={banner02} title="Men" subtitle="Spring 2018" />
              <Banner
                image={banner03}
                title="Accessories"
                subtitle="New Trend"
              />
            </div>
          </div>
        </section>
        <Shop />
      </>
    );
  }
}

export default Home;
