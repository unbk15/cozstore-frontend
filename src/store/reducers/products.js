import * as actionTypes from "./../actions/actionTypes";

const initialState = {
  products: [],
  emptyArray: false,
  errors: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCHING_PRODUCTS:
      return {
        products: action.products,
        emptyArray:  action.emptyArray,
        error: action.error
      };
  }
  return state;
};

export default reducer;
