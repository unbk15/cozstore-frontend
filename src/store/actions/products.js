import axios from "axios";
import * as actionTypes from "./actionTypes";

export const saveProducts = (products, emptyArray, error) => {
  return {
    type: actionTypes.FETCHING_PRODUCTS,
    products, 
    emptyArray, 
    error
  };
};

export const storeProducts = (limit) => {
  return async dispatch => {
    try {
      const products = await axios.get(
        "http://localhost:8000/api/v1/products/"
      );
      dispatch(saveProducts(products.data, false));
    } catch (err) {
      dispatch(saveProducts([], true, true));
    }
  };
};
export const storeFilterProducts = (filterId) => {
  return async dispatch => {
    try {
      const products = await axios.get(
        "http://localhost:8000/api/v1/filter/", {params : {filter: filterId}}
      );
      dispatch(saveProducts(products.data, false));
    } catch (err) {
      console.log(err);
    }
  };
};

export const searchProducts = (searchWord) =>{
  return async dispatch => {
    try {
      const products = await axios.get("http://localhost:8000/api/v1/search/", {params : {searchWord}});
      dispatch(saveProducts(products.data, true));
    }catch(err){
      console.log(err);
    }
  }
}

export const advanceSearchFilter = (filter) =>{
  return async dispatch =>{
      try{
        const products = await axios.get(`http://localhost:8000/api/v1/advance-sort-filter/`, {params : filter}); 
        dispatch(saveProducts(products.data, true));
      }catch(err){
        console.log(err);
      }
  }
}
